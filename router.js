import {getDownload, putGroceryList, deleteGroceryList} from "./download.js"

const routes = (app) => {
  app.route('/download/:id')
      .get(getDownload)
      .put(putGroceryList)
      .delete(deleteGroceryList)
}

export default routes